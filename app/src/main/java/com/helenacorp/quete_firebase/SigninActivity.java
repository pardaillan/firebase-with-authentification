package com.helenacorp.quete_firebase;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SigninActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = SignupActivity.class.getSimpleName();

    private FirebaseAuth mAuth;
    private EditText email_signIn, password_signIn;
    private Button btn_signIn, btn_newaccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        email_signIn = (EditText) findViewById(R.id.email_sigIn);
        password_signIn = (EditText) findViewById(R.id.password_sigIn);
        btn_signIn = (Button) findViewById(R.id.btn_signIn);
        btn_newaccount = (Button) findViewById(R.id.btn_newaccount);

        mAuth = FirebaseAuth.getInstance();

        btn_signIn.setOnClickListener(this);
        btn_newaccount.setOnClickListener(this);

    }

    public void validation() {
        if (email_signIn.getText().length() == 0 ||
                password_signIn.getText().length() == 0) {
            Context context = getApplicationContext();
            CharSequence text = "Veuillez remplir le formulaire complètement,\nsinon grrr";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        } else {
            sigIn(email_signIn.getText().toString(), password_signIn.getText().toString());
        }
    }

    private void sigIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(TAG, "signInWithEmail:success");
                            Intent intent = new Intent(SigninActivity.this, AccountActivity.class);
                            startActivity(intent);

                        } else {
                            // If sign in fails, display a message to the user.
                            //Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(SigninActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }


    @Override
    public void onClick(View v) {
        if (v == btn_signIn) {
            validation();
        }
        if (v == btn_newaccount) {
            Intent intent = new Intent(SigninActivity.this, SignupActivity.class);
            startActivity(intent);
        }
    }
}
