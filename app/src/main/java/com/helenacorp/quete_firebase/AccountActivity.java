package com.helenacorp.quete_firebase;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class AccountActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PICK_IMAGE_REQUEST = 11;
    FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
    private FirebaseAuth mAuth;
    private Button logout, btnphoto, btnUpload, btnDownload;
    private TextView nameProfil, emailProfil;
    private String uId, userPseudo, userEmail;
    private ImageView photoProfil;
    private StorageReference mStorageRef;
    private FirebaseUser user;
    private Uri imageUri;
    private Bitmap bitmap = null;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        if (savedInstanceState != null) {
            bitmap = savedInstanceState.getParcelable("bitmap");
        }

        logout = (Button) findViewById(R.id.btn_logout_profil);
        btnphoto = (Button) findViewById(R.id.btn_choice_profil);
        btnUpload = (Button) findViewById(R.id.btn_upload_profil);
        btnDownload = (Button) findViewById(R.id.btn_download_profil);
        nameProfil = (TextView) findViewById(R.id.name_profil);
        emailProfil = (TextView) findViewById(R.id.email_profil);
        photoProfil = (ImageView) findViewById(R.id.photo_profil);

        logout.setOnClickListener(this);
        btnphoto.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        user = mAuth.getCurrentUser();

        if (user == null) {
            // User is signed out
            finish();
            Intent intent = new Intent(AccountActivity.this, SigninActivity.class);
            startActivity(intent);


        } else {
            // User is signed in
            uId = user.getUid();
            userPseudo = user.getDisplayName();
            userEmail = user.getEmail();
            imageUri = user.getPhotoUrl();
            emailProfil.setText("salut: " + userPseudo);
            nameProfil.setText(userEmail);
            photoProfil.setImageURI(imageUri);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the image bitmap into outState
       /* Glide.with(AccountActivity.this)
                .load(imageUri)
                .into(photoProfil);*/
        // Bitmap bitmap = ((BitmapDrawable)photoProfil.getDrawable()).getBitmap();
        outState.putParcelable("bitmap", imageUri);
    }

    /*  @Override
      public void onRestoreInstanceState (Bundle savedInstanceState) {
          super.onRestoreInstanceState(savedInstanceState);
          // Read the bitmap from the savedInstanceState and set it to the ImageView
          if (savedInstanceState != null){
              Bitmap bitmap = savedInstanceState.getParcelable("bitmap");
              photoProfil.setImageBitmap(bitmap);
          }
      }
  */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(resultCode, requestCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {
            imageUri = data.getData();
            //photoProfil.setImageURI(imageUri);
            try {
                //getting image from gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);

                //Setting image to ImageView
                photoProfil.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void downloadAvatar() {
        // Create a storage reference from our app
        StorageReference storageRef = firebaseStorage.getReference();

        storageRef.child("images/" + uId + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                progressDialog.dismiss();
                Glide.with(AccountActivity.this)
                        .load(uri)
                        .into(photoProfil);
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                progressDialog.dismiss();
                Toast.makeText(AccountActivity.this, exception.toString() + "!!!", Toast.LENGTH_SHORT).show();
            }
        });
       /* final StorageReference usersPic = mStorageRef.child("chat_tete.png");

        usersPic.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(AccountActivity.this)
                        .load(uri)
                        .into(photoProfil);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(AccountActivity.this, "caca", Toast.LENGTH_SHORT).show();
            }
        });*/

    }

    private void uploadImage() {

        if (imageUri != null) {


            StorageReference storageReference = firebaseStorage.getReference();
            StorageReference userPicref = storageReference.child("images/" + uId + ".jpg");
            photoProfil.setDrawingCacheEnabled(true);
            photoProfil.buildDrawingCache();
            Bitmap bitmap = photoProfil.getDrawingCache();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
            byte[] data = baos.toByteArray();
            UploadTask uploadTask = userPicref.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(AccountActivity.this, "Error : " + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    Toast.makeText(AccountActivity.this, "Uploading Done!!!", Toast.LENGTH_SHORT).show();
                    Glide.with(AccountActivity.this)
                            .load(downloadUrl)
                            .into(photoProfil);
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(AccountActivity.this, "Faut choisir", Toast.LENGTH_SHORT).show();
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onClick(View v) {

        if (v == btnphoto) {
            showFileChooser();
        } else if (v == btnUpload) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Uploading....");
            progressDialog.show();
            uploadImage();
        }
        if (v == btnDownload) {
            downloadAvatar();
        }
        if (v == logout) {
            FirebaseAuth.getInstance().signOut();
            Intent i = new Intent(AccountActivity.this, MainActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
